# Project : Aplikace “My Travel”

## Contents
<details> 
<summary>[show | hide]</summary>
1. [Description](https://gitlab.fel.cvut.cz/shcheden/smp-my-travel-app#description)   
<br/>
&#8195;1.1 [Aplikace “My Travel”](https://gitlab.fel.cvut.cz/shcheden/smp-my-travel-app#aplikace-my-travel)
<br/>
&#8195;1.2 [Vize projektu](https://gitlab.fel.cvut.cz/shcheden/smp-my-travel-app#vize-projektu)
<br/>
&#8195;1.3 [Wiki-page](https://gitlab.fel.cvut.cz/shcheden/smp-my-travel-app#wiki-page)
<br/>
2. [Developer Team](https://gitlab.fel.cvut.cz/shcheden/smp-my-travel-app#developer-team)
</details>

## Description
### Aplikace “My Travel” 
Jednoduchá aplikace v telefonu, která vypadá jako kalendář, na kterém turisté mohou vidět jejich exkurze, kde se budou konat, jak dlouho bude trvat, iterinář, stručný program a kdyžtak nějaké věci, co budou na exkurzi potřebovat.  
Také se mohou z exkurze odhlásit a i napsat svému průvodci. 

### Vize projektu
[Vize projektu - Google Docs](https://docs.google.com/presentation/d/1UQ__RlZ0s32Hh549XAkiYCo63pmbzGJjohjHYG-F72s/edit?fbclid=IwAR2wEENcw0y2okO3i_yVlMRuH1zHgH6eJyqqoz770X3HL95UUw8yQzVRn2U#slide=id.p)

### Wiki-page
[Wiki stránka projektu](https://gitlab.fel.cvut.cz/shcheden/smp-my-travel-app/wikis/home)

## Developer Team   
| Member             | e-mail               |
| :--------          | :--------            |
| Jaroslav Seiml     | seimljar@fel.cvut.cz |
| Denis Shcherbakov  | shcheden@fel.cvut.cz |
| Alexanyan Michail  | alexamik@fel.cvut.cz |
| Useinov Artur      | useinart@fel.cvut.cz |